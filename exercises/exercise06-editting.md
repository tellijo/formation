# Exercice 6 : Édition (démarrer de la branche `exercice06`)

## Accéder à la création

1. Dans *home/home.html*, diriger le lien de création vers la page de création définie dans *edit/edit.js*
1. Suivre le lien

## Mettre en commun création et modification

Pas d'exercice ici, juste un peu de réflexion :) 
Tout se passe dans *edit/edit.js*

1. Il y a 2 routes: une pour la création et une pour la modification. Observer la différence et retrouver l'endroit où elle est utilisée.
1. Pourquoi passe-t-on les `$routeParams` en paramètre lors de la création de la tâche ? Se référer à la doc de `$resource`.
1. À quoi servent les watchers un peu partout ? Pourquoi ne sont-ils pas initialisés dans le corps du constructeur, mais dans ces callbacks ?
1. Observer le contrôle d'erreur à la sauvegarde. C'est l'une des manières typiques de le faire dans une application Angular.

## Binder la modification

Le contrôleur est complet, il n'y aura pas besoin de le modifier. 
Tous les bindings demandés peuvent se faire avec les données fournies.

Ignorer la validation pour l'instant dans les *TODO*s de *edit.edit.html*

1. Cacher le formulaire lors du chargement de la tâche, et afficher un "indicateur de chargement".
1. [Binder](https://code.angularjs.org/1.4.7/docs/api/ng/directive/ngModel) les différents champs du formulaire à leur contrepartie dans le scope
1. Le champ `plannedFor` est défini comme une date en HTML. En Angular, depuis la 1.3, ça nécessite d'avoir une `Date` dans la variable.
    1. Créer un filtre `plannedDateAsDate` dans *common/statusFilters.js*, qui convertit `plannedFor` en `Date` si ce n'est pas déjà fait
    1. Le filtre `isLate` créé dans l'exercice 3 s'attendait à un nombre. Le convertir pour qu'il marche tout le temps
    1. Comment s'assurer que ce filtre soit appliqué avant que la valeur soit utilisée dans `ngModel` ? Appliquer dans *edit/edit.js*
    1. `ngModelOptions` permet de spécifier la timezone de la date. Est-ce utile ? Pourquoi ? Quelle timezone faut-il mettre ?
1. Utiliser le filtre `days` défini dans *common/date.js* pour faire des boutons de définition rapide de la date
1. Binder les actions de sauvegarde, suppression et annulation

## Validation de formulaires

On ne veut pas que le formulaire accepte de sauvegarder s'il n'est pas valide.

### Ajouter les contrôles de validité au formulaire

1. Dans *edit/edit.html*, ajouter tous les contrôles de validité intégrés dans Angular :
[required](https://code.angularjs.org/1.4.7/docs/api/ng/directive/input),
[min](https://code.angularjs.org/1.4.7/docs/api/ng/input/input%5Bnumber%5D).

Dans *common/errorsDirectives.js*, il y a une batterie de directives qui vont nous aider à nous assurer que tout va bien et à indiquer les problèmes aux utilisateurs.

### Soumettre uniquement les formulaires valides

Angular s'occupe tout seul de la validité du formulaire. Mais si on ne la vérifie pas, on soumettra quand même !

Implémenter la directive `gtdSubmit` dans *common/errorsDirectives.js*. Elle a les caractéristiques suivantes:

* C'est un attribut dont la valeur est l'expression à evaluer au click si le formulaire est valide (un peu comme ngClick)
* Elle utilise le [FormController](https://code.angularjs.org/1.4.7/docs/api/ng/type/form.FormController), et en particulier sa propriété `$valid` ou `$invalid`, pour décider si le formulaire est valide ou non. Elle doit donc accéder au `formController`
* Elle utilise [$parse](https://code.angularjs.org/1.4.7/docs/api/ng/service/$parse) pour exécuter l'expression passée en attribut.

Remplacer `ngClick` par `gtdSubmit` sur le bouton de soumission du formulaire.

### Mettre un code couleur lorsqu'un champ est invalide

Un champ est toujours valide ou invalide. Cependant pour une bonne UX, il est important de bien choisir quand est-ce qu'on affiche cet état. En particulier:

* Un champ doit rester neutre tant qu'on n'a pas fini une première interaction avec
* Les champs invalides doivent être signalés lorsque l'utilisateur tente de soumettre le formulaire
* Lorsque l'utilisateur corrige une erreur, le marquage d'erreur disparait immédiatement

En Bootstrap, on dénotte une erreur en appliquant la classe `has-error` à la `div.form-group`.

On va donc créer une directive qui fait exactement ça dans les cas notés ci-dessus.

L'information indiquant si un champ est valide ou non vient du [ngModelController][nMC] (propriétés `$valid` et `$invalid`, comme pour le form).
Cependant, ngModel est placé sur l'input, alors qu'on veut poser notre classe sur le form-group !

On a donc besoin de 2 directives (dans *common/errorsDirectives.js*)

* `gtdHasError` va appliquer la classe `has-error` à l'élément dans les bon cas. 
    * Elle va avoir besoin de "trouver" le `ngModelController`. Pour celà elle va définir dans son contrôleur à elle un variable ad-hoc, et laisser une directive fille la remplir.
    * Elle va avoir besoin de savoir si on a déjà essayé de soumettre le formulaire. Il va donc falloir augmenter `gtdSubmit` pour qu'elle stocke cette information dans le `formController`.
* `gtdField` va être placé sur le champ, à côté de `ngModel`. Il va avoir la responsabilité d'enregistrer le `ngModelController` auprès de `gtdHasError` (en utilisant le controlleur de celle-ci)

Il est important de noter qu'aucune de ces directives ne peut se permettre de "polluer" le scope: leur action doit être totalement transparente.

1. Augmenter `gtdSubmit` pour qu'elle "flaggue" une tentative de soumission dans le `formController` (appeller le flag `submitted`).
2. Implémenter `gtdHasError`. 
    * Son contrôleur doit contenir le `ngModelController` (qu'on remplira plus tard).
    * Son linker va définir une fonction capable de décider si le champ est en erreur ou non et d'appliquer ou enlever la classe `has-error` à son élément. 
    * Le linker va aussi watcher les différents paramètres qui déterminent une condition d'erreur (`formController.submitted`, `ngModelController.$valid`, `ngModelController.$touched`) et lancer la fonction de vérification lorsqu'ils changent.
3. Implémenter `gtdField`, qui définit le `ngModelController` dans le contrôleur de `gtdHasError`
4. Placer ces 2 directives dans le formulaire, à la bonne place

Attention, en Angular 1.2, `ngModelController.$touched` n'existait pas. La directive gtdField devait donc aussi ajouter cette fonctionnalité (en écoutant le `blur`).

### Afficher les erreurs

Les erreurs vont décrire à l'utilisateur pourquoi le champ est invalide, et potentiellement lui donner des pistes pour corriger l'erreur.
Ça veut dire qu'une erreur peut contenir des expression bindables, comme des liens, des boutons, ...

On va utiliser la directive [ngMessages](https://code.angularjs.org/1.4.7/docs/api/ngMessages/directive/ngMessages), qui est faite pour cet usage.

On veut afficher les erreurs lorsque `gtdHasError` passe au rouge. On va donc avoir besoin d'un accès à l'information depuis `gtdHasError`.
On doit donc augmenter `gtdHasError` pour publier le statut sur son contrôleur.

On va implémenter la directive `gtdErrors`. Cette directive va fournir à `ngMessages` les erreurs dont elle a besoin.
Pour celà elle va publier sur le scope la valeur de `ngModelController.$errors`, uniquement si les erreurs doivent êtres affichées pour le champ.

1. Augmenter `gtdHasError`. Appeller l'attribut de contrôleur `hasError`.
2. Implémenter `gtdErrors`
    * Le bloc doit être invisible si le champ n'est pas flaggué en erreur, e.g. `gtdErrors` ne doit rien publier.
    * (hint: `gtdOnError` a accès au `ngModelController` par l'intermédiaire du contôleur de `gtdHasError` ! Rien ne se perd, rien ne se crée :). `ngModelController` fournit les types d'erreurs qu'il rencontre dans `$error`)
3. Appliquer `ngMessages` et `gtdErrors` aux champs du formulaire
    * Il ne faut afficher l'erreur que si elle est pertinente. Par exemple pour le champ de durée on a une erreur pour 'required' et une pour 'min'. Il faut donc bien utiliser `ngMessage`
    * Le template Bootstrap pour afficher un texte sous un input est `<p class="help-block"></p>`

### Validation personnalisée

Il y a deux directives de validation de date à implémenter dans *common/date.js*.

Elles vont vous apprendre à utiliser [ngModelController][nMC].

Elles sont destinées à être appliquées sur l'input de `plannedFor`. Gérer aussi les messages d'erreur.

L'une des directives fait de la validation asynchrone. Le serveur fourni est « truqué » pour répondre après 3 secondes.
Il faut prévenir l'utilisateur lorsque une validation est en cours.
Sur le modèle de `gtdError`, créer `gtdPending`, qui affiche un message lors de l'attente d'une validation asynchrone.
La propriété à écouter est `ngModelController.$pending`.
L'appliquer au champ date.

[nMC]: <https://code.angularjs.org/1.4.7/docs/api/ng/type/ngModel.NgModelController>

## Le résultat

Le résultat est un formulaire agréable à utiliser et très bon au niveau UX:
* Il n'interromp pas l'utilisateur en lui faisant peur
* Il réagit immédiatement lorsqu'une erreur est corrigée
* Il protège l'utilisateur d'une mauvaise saisie

Il est aussi extrèmement simple à définir et très clair à la lecture (relisez le code, tout est déclaré et évident dans le HTML)

Par contre, il ne tient pas du tout compte des utilisaturs handicapés (aveugles, …).
Il faudrait rajouter les balises ARIA pour avoir vraiment un comportement parfait.

Et voilà ! Une application certes simpliste, mais complète de GTD.
La correction de cet exercice est sur l'application `complete`.

Pour les plus rapides et les plus fous, tentez l'exercice bonus sur `exerciceX`.
