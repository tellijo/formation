# Exercice 1: Initialiser l'application (démarrer de la branche `exercice01`)

1. [Faire démarrer AngularJS](https://code.angularjs.org/1.4.7/docs/api/ng/directive/ngApp) dans *index.html*
2. [Créer un module](https://code.angularjs.org/1.4.7/docs/api/ng/function/angular.module) `app` dans *app.js* pour gérer l'application
3. Initialiser [le routage](https://code.angularjs.org/1.4.7/docs/api/ngRoute) et une page d'erreur en cas de 404


On a maintenant une application qui tourne, faisons lui faire des choses ! Mergez `exercice02` !
